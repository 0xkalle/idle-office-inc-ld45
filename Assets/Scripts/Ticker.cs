﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Ticker : MonoBehaviour
{
    public string[] staticMessages;
    public TextMeshProUGUI tickerField;

    private int nextStaticMessage;

    public float messageTime;
    // Start is called before the first frame update
    void Start()
    {
        nextStaticMessage = 0;
        StartCoroutine("TickerIt");
    }

    IEnumerator TickerIt() {
        while(true) {
            yield return new WaitForSeconds(messageTime);
            tickerField.text = GetNextStaticMessage();
        }
    }

    private string GetNextStaticMessage() {
        if(staticMessages.Length == 0) return "no new Messages";
        if(nextStaticMessage >= staticMessages.Length) nextStaticMessage = 0;
        nextStaticMessage++;
        return staticMessages[nextStaticMessage -1];
    }
}
