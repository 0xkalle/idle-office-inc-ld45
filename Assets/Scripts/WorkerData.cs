﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkerData
{
    private string name;

    private static string WORKER_KEY = "worker_data_";
    private static string ROBOT_LEVEL_KEY = "robot";

    private static string SHARE_PAYMENT = "payment";
    private static string OFFICE_LEVEL_KEY = "office";

    private int officeLevel;
    private int robotLevel;

    private long lastPayment;

    private long lastSharePayment;

    private Worker robot;

    public void SetRobot(Worker robot) {
        this.robot = robot;
    }

    public Worker GetRobot() => robot;

    public WorkerData(string name) {
        this.name = name;
        officeLevel = PlayerPrefs.GetInt(WORKER_KEY+OFFICE_LEVEL_KEY+name, 0);
        robotLevel = PlayerPrefs.GetInt(WORKER_KEY+ROBOT_LEVEL_KEY+name, 0);
        lastPayment = long.Parse(PlayerPrefs.GetString(WORKER_KEY+SHARE_PAYMENT+name, "0"));
        Debug.Log("LastPayment Loaded "+lastPayment);
    }

    public int GetOfficeLevel() => officeLevel;

    public int GetRobotLevel() => robotLevel;

    public void SharePaymentNow() {
        lastPayment = (long)DateTime.UtcNow.Subtract(new DateTime(1970,1,1)).TotalSeconds;
    }

    public void UpgradeRobot(int level) {
        robotLevel = level;
    }

    public void UpgradeOffice(int level) {
        officeLevel = level;
    }


    public long GetLastPayment() => lastPayment;
    public int GetTimeSinceLastPayment() {
        return (int)((long)DateTime.UtcNow.Subtract(new DateTime(1970,1,1)).TotalSeconds - lastPayment);
    }

    public void SaveIt() {
        PlayerPrefs.SetInt(WORKER_KEY+OFFICE_LEVEL_KEY+name, officeLevel);
        PlayerPrefs.SetInt(WORKER_KEY+ROBOT_LEVEL_KEY+name, robotLevel);
        PlayerPrefs.SetString(WORKER_KEY+SHARE_PAYMENT+name, lastPayment.ToString());
    }

    /*public bool UpgradeOffice() {
        Bank.GetInstance().
    }*/
}
