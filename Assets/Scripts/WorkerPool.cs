﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkerPool : MonoBehaviour
{

    public GameObject workerPrefab;

    private OfficeController officeController;

    private Dictionary<string, Worker> workers;
    private Dictionary<string, Worker> robots;
    // Start is called before the first frame update
    
    private static WorkerPool _instance;

    private void Awake()
    {
        workers = new Dictionary<string, Worker>();
        robots = new Dictionary<string, Worker>();
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }

    public static WorkerPool GetInstance() {
        return _instance;
    }

    void Start() {
        officeController = OfficeController.GetInstance();
    }

    public void JoinPlayer(string name) {
        if(!workers.ContainsKey(name)) {
            Bank.GetInstance().AddPlayer(name);
            WorkPlace wp = officeController.GetEmptyWorkplace();
            Worker toJoin = Instantiate(workerPrefab, new Vector3(-5, wp.transform.position.y + 0.749f, 0), Quaternion.identity).GetComponent<Worker>();
            toJoin.SetUpPlayer(name, wp, false);
            wp.AssignWorker(toJoin);
            workers.Add(name, toJoin);
        }
        if(workers.ContainsKey(name) && workers[name].GetData().GetRobotLevel() > 0) {
            JoinRobot(name, workers[name].GetData());
        }
    }

    public void JoinRobot(string name, WorkerData data) {
        if(!robots.ContainsKey(name)) {
            WorkPlace wp = officeController.GetEmptyWorkplace();
            Worker robot = Instantiate(workerPrefab, new Vector3(-5, wp.transform.position.y + 0.749f, 0), Quaternion.identity).GetComponent<Worker>();
            robot.SetUpPlayer(name, wp, true);
            if(workers.ContainsKey(name))data = workers[name].GetData();
            robot.SetData(data);
            wp.AssignWorker(robot);
            robots.Add(name,robot);
        } else {
            robots[name].ResetTimeout();
            robots[name].SetData(data);
        }
    }

    public void RemovePlayer(string name) {
        if(workers.ContainsKey(name)) workers.Remove(name);
    }
    public void RemoveRobot(string name) {
        if(robots.ContainsKey(name)) robots.Remove(name);
    }

    public void PlayerEarnedMoney(string name, int amount) {
        if(workers.ContainsKey(name)) {
            workers[name].EarnedMoneyByTyping(amount);
            workers[name].ResetTimeout();
        } else JoinPlayer(name);
    }

    public void PlayerBuyShares(string name, int number) {
        if(workers.ContainsKey(name)) {
            workers[name].BuyShares(number);
            workers[name].ResetTimeout();
        } else JoinPlayer(name);
    }

    public void PlayerBuyRobot(string name) {
        if(workers.ContainsKey(name)) {
            workers[name].BuyRobot();
            workers[name].ResetTimeout();
        } else JoinPlayer(name);
    }

    public void PlayerBuyOffice(string name) {
        if(workers.ContainsKey(name)) {
            workers[name].BuyOffice();
            workers[name].ResetTimeout();
        } else JoinPlayer(name);
    }

    public Worker GetPlayer(string name) {
        if(workers.ContainsKey(name))return workers[name];
        return null;
    }

    public Worker GetRobot(string name) {
        if(robots.ContainsKey(name))return robots[name];
        return null;
    }

    public Worker GetRandomPlayer() {
        int count = Random.Range(0, workers.Count);
        int i = 0;
        foreach(KeyValuePair<string, Worker> e in workers) {
            
            if(i == count && e.Value.IsWorking()) return e.Value;
            i++;
        }
        Debug.Log("Worker not found I was"+count);
        return null;
    }

    public Worker GetRandomRobot() {
        int count = Random.Range(0, robots.Count);
        int i = 0;
        foreach(KeyValuePair<string, Worker> e in robots) {
            
            if(i == count && e.Value.IsWorking()) return e.Value;
            i++;
        }
        Debug.Log("Robot not found I was"+count);
        return null;
    }


}
