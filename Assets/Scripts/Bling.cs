﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bling : MonoBehaviour
{
    private Animator animator;

    public AudioClip[] blings;

    private AudioSource audioSource;

    public Color red;
    public Color white;

    private bool ready;
    public TextMeshProUGUI amountText;
    // Start is called before the first frame update

    void Awake() {
        //this.gameObject.SetActive(false);
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        ready = true;
    }

    public bool IsReady() => ready;
    public void BlingAt(int amount, Vector3 location) {
        amountText.text = amount+"";
        if(amount >= 0) amountText.color = white;
        else amountText.color = red;
        ready = false;
        this.transform.position = location;
        //this.gameObject.SetActive(true);
        animator.SetTrigger("bling");
        StartCoroutine("BlingIt");
    }

    IEnumerator BlingIt() {
        yield return new WaitForSeconds(0.1f);
        audioSource.PlayOneShot(blings[Random.Range(0, blings.Length)]);
        yield return new WaitForSeconds(2f);
        ready = true;
        //this.gameObject.SetActive(false);
    }
}
