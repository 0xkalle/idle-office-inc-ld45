﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OfficeController : MonoBehaviour
{
    // Managing workplaces and office size. Assigns workplace to new workers
    public Vector2Int startOfficeSize;

    public GameObject workPlacePrefab; 

    private WorkPlace[,] workplaces;

    private static OfficeController _instance;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
        InstantiateOffice(startOfficeSize);
    }

    public static OfficeController GetInstance() {
        return _instance;
    }

    void Start(){
    }

    void Update() {

    }

    public WorkPlace GetEmptyWorkplace() {
        for(int x = 0; x < workplaces.GetLength(0); x++) {
            for(int y = 0; y < workplaces.GetLength(1); y++) {
                if(!workplaces[x,y].IsAssigned()) return workplaces[x,y];
            }
        }
        AddWorkplaces();
        return workplaces[0, workplaces.GetLength(1)-1];
    }

    private void InstantiateOffice(Vector2Int startSize) {

        workplaces = new WorkPlace[startSize.x, startSize.y];
        for(int x = 0; x < startSize.x; x++) {
            for(int y = 0; y < startSize.y; y++) {
                workplaces[x,y] = InstantiateWorkplace(x, y);
            }
        }

    }

    private WorkPlace InstantiateWorkplace(int x, int y) {
        return Instantiate(workPlacePrefab, new Vector3(x * 2, y * -2, 0), Quaternion.identity).GetComponent<WorkPlace>();
    }

    private void AddWorkplaces() {
        WorkPlace[,] officeNeu = new WorkPlace[workplaces.GetLength(0)+1, workplaces.GetLength(1)+1];
        for(int x = 0; x < workplaces.GetLength(0); x++) {
            for(int y = 0; y < workplaces.GetLength(1); y++) {
                officeNeu[x, y] = workplaces[x,y];
            }
        }

        //New x row
        for(int x = 0; x < workplaces.GetLength(0); x++) {
            officeNeu[x, officeNeu.GetLength(1)-1] = InstantiateWorkplace(x, officeNeu.GetLength(1)-1);
        }

        //new y column plus corner
        for(int y = 0; y <= workplaces.GetLength(1); y++) {
            officeNeu[officeNeu.GetLength(0)-1, y] = InstantiateWorkplace(officeNeu.GetLength(0)-1, y);
        }

        workplaces = officeNeu;
    }

    private void RemoveWorkplaces() {
        if(workplaces.GetLength(0) <= startOfficeSize.x) return;
        WorkPlace[,] officeNeu = new WorkPlace[workplaces.GetLength(0)-1, workplaces.GetLength(1)-1];
        for(int x = 0; x < workplaces.GetLength(0); x++) {
            for(int y = 0; y < workplaces.GetLength(1); y++) {
                if(x >= workplaces.GetLength(0)-1 || y >= workplaces.GetLength(1)-1) workplaces[x,y].RemoveWorkplace();
                else officeNeu[x, y] = workplaces[x,y];
            }
        }
        workplaces = officeNeu;
    }

    public Vector2 GetTopLeft() {
        return new Vector2(-1f, 1f);
    }
    public Vector2 GetBottomRight() {
        return GetTopLeft()+new Vector2(workplaces.GetLength(0)*2f, -1f*workplaces.GetLength(1)*2f);
    }

    public Vector2 GetOfficeSize() {
       return new Vector2(workplaces.GetLength(0)*2f,workplaces.GetLength(1)*2f);
    }

    public Vector2 GetOfficeCenter() {
        return GetTopLeft()+new Vector2(workplaces.GetLength(0)*1f, -1f*workplaces.GetLength(1));
    }




}
