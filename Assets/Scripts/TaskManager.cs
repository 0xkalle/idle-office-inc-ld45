﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TaskManager : MonoBehaviour
{
    public float timeDelay;
    public float taskTime;
    public int taskCount;
    public float taskGenerationTime;

    public TextMeshProUGUI[] taskTexts;

    private Dictionary<string, Task> activeTasks;
    private Dictionary<string, int> answerValues;

    private static TaskManager _instance;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }

    public static TaskManager GetInstance() {
        return _instance;
    }

    // Start is called before the first frame update
    void Start()
    {
        activeTasks = new Dictionary<string, Task>();
        answerValues = new Dictionary<string, int>();
        RefreshTaskBoard();
        StartCoroutine("TaskAdder");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void RefreshTaskBoard() {
        int count = 0;
        foreach(KeyValuePair<string, Task> e in activeTasks) {
            taskTexts[count].text = e.Value.TaskToString();
            count++;
            if(count >= taskTexts.Length) break;
        }
        for(int i = count; i < taskTexts.Length; i++) {
            taskTexts[i].text = "";
        }
    }

    

    IEnumerator TaskAdder() {
        while(true) {
            AddTask();
            yield return new WaitForSeconds(taskGenerationTime);
        }
    }

    private void AddTask() {
        if(activeTasks.Count >= taskCount) return;
        Task toAdd = TaskGenerator.GenerateTask();
        if(!activeTasks.ContainsKey(toAdd.answer) && !answerValues.ContainsKey(toAdd.answer)) {
            activeTasks.Add(toAdd.answer, toAdd);
            answerValues.Add(toAdd.answer, toAdd.value);
            StartCoroutine(RemoveTask(toAdd.answer));
            RefreshTaskBoard();
            Debug.Log("Task "+toAdd.answer+" added. (Task Count = "+activeTasks.Count+")");
        }
    }

    IEnumerator RemoveTask(string answer) {
        yield return new WaitForSeconds(taskTime);
        activeTasks.Remove(answer);
        RefreshTaskBoard();
        Debug.Log("Task "+answer+" removed.");
        yield return new WaitForSeconds(timeDelay);
        answerValues.Remove(answer);
        Debug.Log("Task "+answer+" completely removed.");
        yield return null;
    }

    public int CheckAnswer(string answer) {
        if(answerValues.ContainsKey(answer))return answerValues[answer];
        else return 0;
    }
}
