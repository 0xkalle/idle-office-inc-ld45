﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bank : MonoBehaviour
{
    private static Bank _instance;

    public float highscoreRefreshInterval;

    private static string MONEYS_KEY = "worker_moneys";
    private static string SHARES_KEY = "worker_shares";

    private static string COMPANY_KEY = "company_moneys";

    private WorkerPool workerPool;
    
    

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }

        LoadIt();
        StartCoroutine("HighscoreRefreseher");
    }

    void Start() {
        workerPool = WorkerPool.GetInstance();
    }

    void OnApplicationPause(bool pause) {
        if(pause) SaveIt();
    }
    void OnApplicationQuit() {
        SaveIt();
    }


    public static Bank GetInstance() {
        return _instance;
        
    }

    private int GetOfficeUpgradeCost(int level) {
        return (level+1)*(level+1)*1000;
    }

    private int GetRobotUpgradeCost(int level) {
        return (int)Mathf.Pow(2,level)*1000;
    }

    public bool BuyOfficeUpgrade(string name, int level) {
        return SubMoney(name, GetOfficeUpgradeCost(level));
    }

    public bool BuyRobotUpgrade(string name, int level) {
        return SubMoney(name, GetRobotUpgradeCost(level));
    }

    private bool SubMoney(string name, int amount) {
        if(moneys.ContainsKey(name) && moneys[name] > amount) {
            moneys[name] -= amount;
            Worker w = workerPool.GetPlayer(name);
            if(w != null) BlingPool.GetInstance().BlingAt(-amount, w.transform.position);
            return true;
        }
        return false;
    }


    private Dictionary<string, long> moneys;
    private Dictionary<string, int> shares;

    private long companyMoney;

    public int sharePrice;
    public int shareValue;

    public float shareInterval;

    public TextMeshProUGUI companyValueText;

    public TextMeshProUGUI[] mwws;

    private List<KeyValuePair<string, long>> highscore;

    public void AddPlayer(string name) {
        if(!moneys.ContainsKey(name)) {
            moneys.Add(name, 0);
            shares.Add(name, 0);
        }
    }

    IEnumerator HighscoreRefreseher() {
        while(true) {
            CalculateHighscore();
            yield return new WaitForSeconds(highscoreRefreshInterval);
        }
    }

    private void CalculateHighscore() {
        List<KeyValuePair<string, long>> nhs = new List<KeyValuePair<string, long>>();
        foreach(string player in moneys.Keys){
            nhs.Add(new KeyValuePair<string, long>(player,GetPlayerValue(player)));
        }
        nhs.Sort((b,a)=>a.Value.CompareTo(b.Value));
        highscore = nhs;
        RefreshHighscoreDisplay();
    }

    private void RefreshHighscoreDisplay() {
        for(int i = 0; i < mwws.Length && i < highscore.Count; i++) {
            mwws[i].text = (i+1)+". "+highscore[i].Key+"\n"+highscore[i].Value;
        }
    }

    public long GetPlayerMoney(string name) {
        if(moneys.ContainsKey(name)) return moneys[name];
        else return 0;
    }

    public long GetPlayerValue(string name) {
        if(moneys.ContainsKey(name)) return moneys[name];
        else return 0;
    }

    public int GetPlayerShares(string name) {
        if(shares.ContainsKey(name)) return shares[name];
        return 0;
    }

    public bool BuyShares(string name, int number) {
        if(SubMoney(name, number*sharePrice)) {
            shares[name] += number;
            return true;
        }
        return false;
    }

    public float GetShareInterval() => shareInterval;
    public int GetShareValue() => shareValue;

    public void MoneyEarned(string name, int amount) {
        if(moneys.ContainsKey(name)) {
            moneys[name] += amount;
            companyMoney += amount;
            companyValueText.text =  "ld45 earned "+companyMoney;
        }
    }

    private void SaveIt() {
        PlayerPrefs.SetString(COMPANY_KEY, companyMoney.ToString());
        PlayerPrefs.SetString(MONEYS_KEY, MoneysToString(moneys));
        PlayerPrefs.SetString(SHARES_KEY, SharesToString(shares));

    }

    private string MoneysToString(Dictionary<string, long> dict){
        string erg = "";
        foreach(KeyValuePair<string, long> e in dict) {
            erg += e.Key+":"+e.Value.ToString()+";";
        }
        return erg;
    }

    private Dictionary<string, long> StringToMoneys(string str){
        string[] pairs = str.Split(';');
        string[] cPair;
        Dictionary<string, long> dict = new Dictionary<string, long>();
        for(int i = 0; i < pairs.Length; i++) {
            if(pairs[i] != null && pairs[i] != "") {
                cPair = pairs[i].Split(':');
                dict.Add(cPair[0], long.Parse(cPair[1]));
            }
        }
        return dict;
    }

    private string SharesToString(Dictionary<string, int> dict){
        string erg = "";
        foreach(KeyValuePair<string, int> e in dict) {
            erg += e.Key+":"+e.Value.ToString()+";";
        }
        return erg;
    }

    private Dictionary<string, int> StringToShares(string str){
        string[] pairs = str.Split(';');
        string[] cPair;
        Dictionary<string, int> dict = new Dictionary<string, int>();
        for(int i = 0; i < pairs.Length; i++) {
            if(pairs[i] != null && pairs[i] != "") {
                cPair = pairs[i].Split(':');
                dict.Add(cPair[0], int.Parse(cPair[1]));
            }
        }
        return dict;
    }

    private void LoadIt() {
        companyMoney = long.Parse(PlayerPrefs.GetString(COMPANY_KEY, "0"));
        shares = StringToShares(PlayerPrefs.GetString(SHARES_KEY, ""));
        moneys = StringToMoneys(PlayerPrefs.GetString(MONEYS_KEY, ""));
        companyValueText.text = "ld45 earned "+companyMoney;
    }


}
