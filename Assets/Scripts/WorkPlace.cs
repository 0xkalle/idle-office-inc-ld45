﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkPlace : MonoBehaviour
{
    private Worker worker;

    public SpriteRenderer ground, chair, table;

    public Sprite[] grounds, chairs, tables;
    

    void Start() {
        SetLevel(0);
    }
    public bool IsAssigned() => worker != null;

    public void RemoveWorkplace() {
        Destroy(this.gameObject);
    }

    public void SetLevel(int level) {
        if(level > grounds.Length -1) level = grounds.Length -1;
        ground.sprite = grounds[level];
        chair.sprite = chairs[level];
        table.sprite = tables[level];
    }

    public void AssignWorker(Worker worker) {
        this.worker = worker;
    }

    public void UnassignWorker() {
        this.worker = null;
        SetLevel(0);
    }
}
