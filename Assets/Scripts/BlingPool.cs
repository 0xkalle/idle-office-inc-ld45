﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlingPool : MonoBehaviour
{
    public GameObject blingPrefab;
    private Bling[] blingPool;
    public int poolStep;

    private int nextI;

    private static BlingPool _instance;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }

    public static BlingPool GetInstance() {
        return _instance;
    }

    // Start is called before the first frame update
    void Start()
    {
        blingPool = new Bling[0];
        ResizePool();
        nextI = 0;
    }

    void ResizePool() {
        int newSize = blingPool.Length + poolStep;
        nextI = blingPool.Length;
        Bling[] pool2 = new Bling[newSize];
        for(int i = 0; i < blingPool.Length; i++) {
            pool2[i] = blingPool[i];
        }
        for(int i = blingPool.Length; i < pool2.Length; i++) {
            pool2[i] = Instantiate(blingPrefab, Vector3.zero, Quaternion.identity).GetComponent<Bling>();
        }
        blingPool = pool2;
    }

    public void BlingAt(int amount, Vector3 location) {
        GetUnusedBling().BlingAt(amount, location);
    }

    public Bling GetUnusedBling() {
        int i = 0;
        while(i < blingPool.Length) {
            i++;
            nextI++;
            if(nextI >= blingPool.Length) nextI = 0;
            if(blingPool[nextI].IsReady()) {
                Debug.Log("Bling "+nextI+" selected.");
                return blingPool[nextI];
            } 
        }
        ResizePool();
        return blingPool[nextI];
    }
}
