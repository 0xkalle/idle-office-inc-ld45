﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public float minSize;
    public float maxSize;

    private Camera mainCamera;
    private OfficeController officeController;
    public Vector3 cameraOffset;

    private Vector2 nextTarget;
    private float nextZoomTarget;

    public float zoomFactor;
    public float moveForce;

    private bool targetReached;
    private Rigidbody2D rb;


    // Start is called before the first frame update
    void Start()
    {
        targetReached = true;
        rb = GetComponent<Rigidbody2D>();
        mainCamera = GetComponent<Camera>();
        officeController = OfficeController.GetInstance();
        //Debug.Log("Office Center is "+officeController.GetOfficeCenter());
        nextZoomTarget = mainCamera.orthographicSize;
        transform.position = new Vector3(0f,0f,transform.position.z)+(Vector3)officeController.GetOfficeCenter()+cameraOffset;
        StartCoroutine("CameraDynamic");
    }

    IEnumerator CameraDynamic() {
        while(true) {
            SetNextCameraTarget();
            yield return new WaitForSeconds(5f);
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        //MOVE
        if(!targetReached) {
            float distance = Vector2.Distance((Vector2) this.transform.position, nextTarget);
            if(distance < 1f) targetReached = true;
            Vector2 force = (((Vector2) this.transform.position) - nextTarget).normalized * Mathf.Clamp(distance, 0f, moveForce) * -1f;
            rb.AddForce(force);
        }
        //ZOOM
        if(mainCamera.orthographicSize > nextZoomTarget) {
            float nextSize = mainCamera.orthographicSize - Time.deltaTime*zoomFactor;
            if(nextSize <= nextZoomTarget) nextSize = nextZoomTarget;
            mainCamera.orthographicSize = nextSize;
        }else if(mainCamera.orthographicSize < nextZoomTarget) {
            float nextSize = mainCamera.orthographicSize + Time.deltaTime*zoomFactor;
            if(nextSize >= nextZoomTarget) nextSize = nextZoomTarget;
            mainCamera.orthographicSize = nextSize;
        }
    }

    void SetNextCameraTarget() {
        targetReached = false;
        nextZoomTarget = Random.Range(minSize, GetMaxOrtSize());
        Vector2 oSize = officeController.GetOfficeSize();
        Worker targetWorker = null;
        if(Random.Range(0,2)>0)WorkerPool.GetInstance().GetRandomRobot();
        else targetWorker = WorkerPool.GetInstance().GetRandomPlayer();
        if(targetWorker != null) nextTarget = (Vector2) targetWorker.transform.position + (Vector2) cameraOffset;
        else nextTarget = officeController.GetTopLeft() + new Vector2(Random.Range(1f, oSize.x -1f), Random.Range((oSize.y-1f) * -1f,-1f)) + (Vector2) cameraOffset;
        //Debug.Log("NextTargetIs "+nextTarget);
    }

    private float GetMaxOrtSize() {
        return Mathf.Clamp(officeController.GetOfficeSize().y / 1.5f, minSize, maxSize);
    }




}
