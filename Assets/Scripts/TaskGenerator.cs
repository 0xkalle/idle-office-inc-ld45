﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TaskGenerator
{
    public static string[] words = {"about", "undefined", "index out of", "think think", "pros write a bot", "i am so tired", "are we there yet?", "this is sooo much fun", "more words please", "keyboard will catch fire", "godlike", "robots will win", "master","hacker", "connecting", "meta", "cloud infrastructure","make her happy", "all the spam", "spam spam spam", "make him happy", "boobs :D","ludum dare", "gamejam", "merge", "omg", "work sucks", "one world", "love", "wtf", "duck", "start with nothing", "cup of tea", "coffe", "cola", "guten tag", "more money please", "money is good", "Pogchamp", "loool", "rofl", "Kappa", "NotLikeThis", "BibleThumb"};
    public static Task GenerateTask() {
        int number = Random.Range(0, words.Length-1);
        return new Task("type '"+words[number]+"'", ""+words[number], words[number].Length * Random.Range(2,5));
    }
}
