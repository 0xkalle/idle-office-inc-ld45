﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Worker : MonoBehaviour
{
    
    private string twitchUserName;

    public int idleAmount;

    public float idleFrequence;
    public float timeoutSecs;
    public float robotTimeOutSecs;

    private float timeoutAt;

    public float walkingSpeed;

    public GameObject stats;

    public Sprite[] workerHeads;
    public Sprite[] robotHeads;
    public Sprite[] workerArms;
    public Sprite[] robotArms;
    public Sprite[] workerBodies;
    public Sprite[] robotBodies;
    
    public SpriteRenderer body, armL, armR, head;

    private bool isRobot;
    private WorkPlace workPlace;
    public TextMeshProUGUI playerName;
    public TextMeshProUGUI moneyText;
    public TextMeshProUGUI sharesText;

    private Animator animator;
    private AudioSource audioSource;

    public AudioClip[] typeSounds, hiM, hiW;

    public float botInterval;
    public int botBaseIncome;

    private bool isReady;
    private bool isTimeout;
    

    private Bank bank;

    private WorkerData data;

    void Awake() {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        workPlace = null;
    }

    void Start() {
        bank = Bank.GetInstance();
        RefreshCounter();
        ResetTimeout();
        isReady = false;
        isTimeout = false;
        stats.SetActive(false);
    }

    void SetSprites(bool isRobot) {
        if(isRobot) {
            Debug.Log("ROBOT LEBEL IS"+ data.GetRobotLevel());
            int i = data.GetRobotLevel()-1;
            if(i >= robotBodies.Length) i = robotBodies.Length -1;
            body.sprite = robotBodies[i];
            head.sprite = robotHeads[i];
            armL.sprite = robotArms[i];
            armR.sprite = robotArms[i];
            
        } else {
            int armI = Random.Range(0, workerArms.Length);
            body.sprite = workerBodies[armI];
            head.sprite = workerHeads[armI];
            armL.sprite = workerArms[armI];
            armR.sprite = workerArms[armI];
            if(armI >=4 && armI <= 6) audioSource.PlayOneShot(hiW[Random.Range(0, hiW.Length)]);
            else audioSource.PlayOneShot(hiM[Random.Range(0, hiM.Length)], 1f);
        }
    }



    void Update() {
        if(!isReady) {
            this.transform.position = new Vector3(this.transform.position.x + walkingSpeed*Time.deltaTime, this.transform.position.y, 0);
            if(workPlace != null && this.transform.position.x >= workPlace.transform.position.x) {
                isReady = true;
                this.transform.position = workPlace.transform.position;
                if(!isRobot) {
                    stats.SetActive(true);
                    StartShareEarning();
                    StartCoroutine("IdleMoney");
                    workPlace.SetLevel(data.GetOfficeLevel());
                } else {
                    StartCoroutine("RobotIncome");
                    workPlace.SetLevel(1);
                }
            }
        }

        if(isTimeout) {
            this.transform.position = new Vector3(this.transform.position.x - walkingSpeed*Time.deltaTime, this.transform.position.y, 0);
            if(this.transform.position.x < -10) {
                SaveData();
                Destroy(this.gameObject);
            }
        }
        
        if(!isTimeout && Time.time > timeoutAt) TimeoutSelf();
    }

    IEnumerator IdleMoney() {
        while(true) {
            yield return new WaitForSeconds(idleFrequence);
            EarnedMoney(idleAmount);
        }
    }
    IEnumerator RobotIncome() {
        while(true) {
            float interval = botInterval - data.GetRobotLevel()/2f;
            if(interval < 2f) interval = 2f;
            yield return new WaitForSeconds(interval);
            BotEarnedMoneyByTyping(botBaseIncome);
        }
    }

    private void StartShareEarning() {
        int shares = bank.GetPlayerShares(twitchUserName);
        if(shares > 0) {
            int amount = bank.GetShareValue() * (int)Mathf.Floor( data.GetTimeSinceLastPayment() / bank.GetShareInterval());
            EarnByShares(amount);
        }
        data.SharePaymentNow();
        StartCoroutine("ShareEarning");
    }

    IEnumerator ShareEarning() {
        
        yield return new WaitForSeconds(Random.Range(2f, 5f));
        while(true) {
            yield return new WaitForSeconds(bank.GetShareInterval());
            EarnByShares(bank.GetPlayerShares(twitchUserName) * bank.GetShareValue());
        }
    }

    IEnumerator TypeNow(int amount) {
        animator.SetTrigger("typing");
        audioSource.PlayOneShot(typeSounds[Random.Range(0, typeSounds.Length)], 0.5f);
        yield return new WaitForSeconds(1f);
        EarnedMoney(amount);
        animator.SetTrigger("idle");
    }

    IEnumerator TypeNowBot(int amount) {
        animator.SetTrigger("typing");
        audioSource.PlayOneShot(typeSounds[Random.Range(0, typeSounds.Length)], 0.5f);
        yield return new WaitForSeconds(1f);
        BotEarnedMoney(amount);
        animator.SetTrigger("idle");
    }

    public void RefreshCounter() {
        moneyText.text = bank.GetPlayerMoney(twitchUserName)+"";
        sharesText.text = bank.GetPlayerShares(twitchUserName)+""; 
    }

    private void RefreshName() {
        if(!isRobot)playerName.text = twitchUserName+"("+(data.GetOfficeLevel()+1)+")";
        else playerName.text = twitchUserName+"'s bot("+data.GetRobotLevel()+")";
    }

    public void SetUpPlayer(string userName, WorkPlace workPlace, bool isRobot) {
        this.workPlace = workPlace;
        this.isRobot = isRobot;
        twitchUserName = userName;
        data = new WorkerData(userName);
        SetSprites(isRobot);
        RefreshName();
    }

    public bool IsWorking() => isReady && !isTimeout;

    public void ResetTimeout() {
        if(!isRobot) {
            timeoutAt = Time.time + timeoutSecs;
            if(data.GetRobot() != null)data.GetRobot().ResetTimeout();
        }
        else timeoutAt = Time.time + robotTimeOutSecs;
    }

    public void EarnedMoneyByTyping(int amount) {
        IEnumerator typeMoney = TypeNow(amount);
        StartCoroutine(typeMoney);
    }

    public void BotEarnedMoneyByTyping(int amount) {
        IEnumerator typeMoney = TypeNowBot(amount);
        StartCoroutine(typeMoney);
    }

    public void BuyShares(int number) {
        if(bank.BuyShares(twitchUserName, number)){
            Debug.Log("Shares bought");
        }

    }

    public void BuyRobot() {
        if(bank.BuyRobotUpgrade(twitchUserName, data.GetRobotLevel()+1)) {
            data.UpgradeRobot(data.GetRobotLevel()+1);
            if(data.GetRobotLevel() <= 1)WorkerPool.GetInstance().JoinRobot(twitchUserName, data);
            else {
                Worker robot = WorkerPool.GetInstance().GetRobot(twitchUserName);
                if(robot != null) robot.RobotUpgraded();
            }
        }
    }

    public void BuyOffice() {
        if(bank.BuyOfficeUpgrade(twitchUserName, data.GetOfficeLevel()+1)) {
            data.UpgradeOffice(data.GetOfficeLevel()+1);
            OfficeUpgraded();
        }
    }

    public void SetData(WorkerData data) {
        //Only called on robots
        this.data = data;
        data.SetRobot(this);
        RefreshName();
        SetSprites(isRobot);
    }

    public void RobotUpgraded() {
        // ROBOT GOT UPGRADED
        TimeoutSelf();
        WorkerPool.GetInstance().JoinRobot(twitchUserName, data);
        
    }

    public void OfficeUpgraded() {
        // ROBOT GOT UPGRADED
        Debug.Log("Office upgraded called");
        RefreshName();
        workPlace.SetLevel(data.GetOfficeLevel());
    }


    public WorkerData GetData() => data;

    public void EarnByShares(int amount) {
        if(amount <= 0) return;
        data.SharePaymentNow();
        EarnedMoney(amount);    
    }

    public void EarnedMoney(int amount) {
        amount = amount * (data.GetOfficeLevel()+1);
        BlingPool.GetInstance().BlingAt(amount, this.transform.position);
        bank.MoneyEarned(twitchUserName, amount);
        RefreshCounter();
        Debug.Log("Player "+twitchUserName+" earned "+ amount+" (now "+bank.GetPlayerMoney(twitchUserName)+").");
    }
    public void BotEarnedMoney(int amount) {
        amount = amount * data.GetRobotLevel();
        BlingPool.GetInstance().BlingAt(amount, this.transform.position);
        bank.MoneyEarned(twitchUserName, amount);
        Debug.Log("PlayerBot "+twitchUserName+" earned "+ amount+" (now "+bank.GetPlayerMoney(twitchUserName)+").");
    }

    private void TimeoutSelf() {
        //TODO Walk away animation
        isTimeout = true;
        stats.SetActive(false);
        this.transform.position = this.transform.position + Vector3.up * 0.749f;
        workPlace.UnassignWorker();
        if(!isRobot) WorkerPool.GetInstance().RemovePlayer(twitchUserName);
        else WorkerPool.GetInstance().RemoveRobot(twitchUserName);
    }

    private void SaveData() {
        data.SaveIt();
    }

    void OnApplicationPause(bool pause) {
        if(pause) SaveData();
    }
    void OnApplicationQuit() {
        SaveData();
    }
}
