﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task
{
    public string desc;
    public string answer;
    public int value;

    public Task(string desc, string answer, int value) {
        this.desc = desc;
        this.answer = answer;
        this.value = value;
    }

    public string TaskToString() {
        return "-"+desc+" ("+value+")";
    }
}
