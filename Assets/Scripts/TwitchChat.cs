﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.ComponentModel;
using System.Net.Sockets;
using System.IO;
using UnityEngine.UI;

public class TwitchChat : MonoBehaviour {

    private TcpClient twitchClient;
    private StreamReader reader;
    private StreamWriter writer;

    public string username, password, channelName; //Get the password from https://twitchapps.com/tmi
    private static TwitchChat _instance;

    private WorkerPool workerPool;

    private TaskManager taskManager;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }

    public static TwitchChat GetInstance() {
        return _instance;
    }


	void Start () {
        workerPool = WorkerPool.GetInstance();
        taskManager = TaskManager.GetInstance();
        Connect();
    }
	
	void Update () {
        if (!twitchClient.Connected)
        {
            Connect();
        }
        

        ReadChat();
    }

    private void Connect()
    {
        twitchClient = new TcpClient("irc.chat.twitch.tv", 6667);
        reader = new StreamReader(twitchClient.GetStream());
        writer = new StreamWriter(twitchClient.GetStream());

        writer.WriteLine("PASS " + password);
        writer.WriteLine("NICK " + username);
        writer.WriteLine("USER " + username + " 8 * :" + username);
        writer.WriteLine("JOIN #" + channelName);
        writer.Flush();
    }

    private void ReadChat()
    {
        if(twitchClient.Available > 0)
        {
            string message = reader.ReadLine(); //Read in the current message
            //Debug.Log("Message: "+message);

            if (message.Contains("PRIVMSG"))
            {
                //Get the users name by splitting it from the string
                var splitPoint = message.IndexOf("!", 1);
                var chatName = message.Substring(0, splitPoint);
                chatName = chatName.Substring(1);

                //Get the users message by splitting it from the string
                splitPoint = message.IndexOf(":", 1);
                message = message.Substring(splitPoint + 1);
                //print(String.Format("{0}: {1}", chatName, message));
                //chatBox.text = chatBox.text + "\n" + String.Format("{0}: {1}", chatName, message);

                //Run the instructions to control the game!
                GameInputs(chatName, message);
            } if(message == "PING :tmi.twitch.tv") {
                Debug.Log("PONG NOW");
                writer.WriteLine("PONG :tmi.twitch.tv");
                writer.Flush();
            }
        }
    }

    private void GameInputs(string player, string chatInput)
    {
        if(chatInput == "!join") {
            // Player should join if not already there
            workerPool.JoinPlayer(player);
        } else if(chatInput == "!shares") {
            workerPool.PlayerBuyShares(player, 1);
        } else if(chatInput == "!office") {
            workerPool.PlayerBuyOffice(player);
        } else if(chatInput == "!robot") {
            workerPool.PlayerBuyRobot(player);
        } else {
            // Other input
            int amount = taskManager.CheckAnswer(chatInput);
            if(amount > 0) workerPool.PlayerEarnedMoney(player, amount);
        }
    }

    public void WritePlayerStats(Worker worker) {
        //LATER
    }
}

